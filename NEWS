#+TITLE: Guile Goblins NEWS – history of user-visible changes

* Releases
** v0.14
*** Highlights
- New libp2p netlayer
- Intra-vat persistence support
- Persistence support in netlayers
*** New features and improvements
**** New libp2p netlayer

Libp2p is a peer-to-peer network stack which backs IPFS, Filecoin and other
peer-to-peer applications. You can now use libp2p as a netlayer within Goblins,
by spawning the =^libp2p-netlayer= actor from =(goblins ocapn netlayer libp2p)=.
Since there isn't a libp2p library directly in Guile, the netlayer relies upon a
daemon written in Go, you can find that here:

https://gitlab.com/spritely/go-libp2p-daemon

**** Intra-vat persistence support

Many improvements have come to the persistence system in this release, perhaps
the most significant is allowing persisting far references, this is references
to objects which live on other local vats. This is done by a new actor,
=^persistence-registry= which each vat registers with and coordinates the
restoration of each vat asynchronously.

Usually in its own vat (does not need to be persistence aware), you spawn a
persistence registry, you then pass this into each persistent vat using the
=#:persistence-registry= keyword. Since vats can be spawned at any time during
the program's lifetime, the persistence system resolves far references to
promises which may resolve when the coordination has completed.

**** Persistence support in netlayers

Netlayers now support the persistence system. Goblins now handles storing and
providing the secrets and other information a netlayer might need to persist the
connection. To enable this just spawn the netlayer within a persistent vat and
provide a store and everything will be handled automatically, just like working
with any other persistence aware actors.

*** Bug fixes

- Fixes issue where multiple connections would be made when enlivening two
  sturdyrefs to the same node when no connection existed prior.
- Vats with logging disabled no longer take a snapshot each churn.
- The =all-of*= joiner will now resolve when given an empty list of promises.
- =on-sever= promises will be immediately broken if the CapTP connection has
  already been severed.
- Fix issue where IO actor procedures would assume optional init and cleanup
  procedures where always provided.

*** Known issues
**** Prelay connections are unencrypted and unauthenticated

Messages sent through the Prelay netlayer can be viewed by the relay server,
modified and new messages inserted by the server or silently removed and thus
not delivered by the Prelay netlayer.

This means that you should only use the Prelay netlayers where you can place
high trust in the relaying server.

**** Lack of dialback within CapTP

When a node connects to another node in Goblins it provides the location where
it's accessable (i.e. the OCapN node address). One feature of OCapN is to ensure
only one connection exists to any given node, since the address provided by a
new incoming connection is never authenticated, this could lead to a denial of
service by a malicious actor claiming to be a node it is not.

** v0.13
*** Highlights
- New persistence system
- New IO actors
*** New features and improvements
- Refactored syrup sets into gsets located in =(goblins ghash)=
- =(goblins actor-lib selfish-spawn)= is now documented.
**** New Persistence System

The new persistence system provides a mechanism to safely serialize
and restore actors within a graph. Actors defined with the new
=define-actor= macro, or by using the manual mechanism by specifying a
self-portrait function are able to be serialized, saved and then
brought back. This makes handling building systems which need to
shutdown a easy and safe saving mechanism.

Due to how the persistence system works, it also allows for upgrading
of actor behavior and state. It also allows for a more powerful live
hacking experience by allowing object definitions to be changed and
then all objects of that type to be upgraded to the new definition
while keeping their references the same.

The system works with both actormaps and vats and comes with a syrup
file based store and a in-memory store.

**** IO Actors

A new actor-lib library (=(goblins actor-lib io)=) has been added
which makes interacting with external IO resources (or possibly other
resources) much easier and more natural within Goblins. It abstracts
the Fibers details and provides an actor which you're able to message
and receive promises like you'd expect from Goblins' actors.

*** Bug fixes
- Fixes possible problem with the =^nonce-registry= actor not having
  time consistent lookup times.
- Fix race condition during in churns when using =call-with-vat=.
- Fix timeout issue in some tests caused by
  =resolve-vow-and-return-result=
- Fix issue sending =*unspecified*= accross CapTP.
- Fix ordering issues for expected and actual in tests.
- Improves error messages when invalid OCapN URIs are parsed.
*** API Changes
- The function to swap the swappable proxy from =(goblins actor-lib
  swappable)= has now become an actor.
- When supplying custom sealers to =(goblins actor-lib sealers)= they
  must be defined in a known sealers alist.
*** Known issues
- [[https://gitlab.com/spritely/guile-goblins/-/issues/191][#191]] Objects defined with =define-actor= do not retain their docstrings
- selfish-spawn does not work with new persistence system
** v0.12
*** Highlights

- New Prelay netlayer
- New TCP-TLS netlayer
- Various fixes and enhancements to CapTP support
- A new Queue (FIFO) actor in our actor-lib library

*** New features and improvements
**** Prelay Netlayer

The Prelay netlayer (the "preview of relay" netlayer) is a new netlayer using
the existing netlayers, it's designed to have a relay server and relay
client. This works especially well with, for example the new TCP-TLS netlayer as
often users will be behind firewalls and routers which require special
configuration to allow for peer-to-peer connections.

This new netlayer hopefully will help connect more nodes who otherwise couldn't
have connected to each other. It's currently an early version of itself with
some limitations, those being that traffic is unencrypted and unauthenticated,
something akin to IRC or vanilla ActivityPub. It currently requires trusting the
relay server, however in the near future we will be looking to address those
limitations.

**** TCP-TLS Netlayer

This netlayer supports connection over TCP sockets with encryption built in with
TLS. Unlike the tor netlayer which requires extra software (the Tor daemon) and
configuration, this allows for both simpler and quicker communication between
two nodes. Encryption occurs with self signed certificates. This netlayer works
particularly well with the new Prelay netlayer as often making peer-to-peer
connections is difficult due to firewall or router configurations.

**** Improved CapTP support

There has been multiple fixes and enhancements to CapTP support, which are:

- Crossed hellos mitigation (when two nodes try to connect to each other at the
  same time)
- Removal of the now deprecated =op:bootstrap= operation (this is now done by
  always exporting the bootstrap object at position =0=).
- Verification of the handoff count on Third Party Handoff certificates.

In addition, the [[https://github.com/ocapn/ocapn-test-suite][OCapN test suite]] can be now run. There is the test support
added to examples/ocapn-test-suite.scm which when run provide a OCapN node
address which can be provided with the test suite to allow the suite to run.

**** A new Queue actor

This new queue actor exists =(goblins actor-lib queue)= and represents a queue
(i.e. FIFO) where new items can be queued or removed. Please refer to the new
documentation to see how to use this.

*** Bug fixes

- Fix =live-guile.sh= live hacking script when certain Geiser incompatible
  configurations were in the =.guile= config file.
- REPL test is fixes when locale has a limited character set.
- Fix bug in =install-netlayer= method on mycapn which used an invalid method
  causing it not to work.

*** Known issues
**** Prelay connections are unencrypted and unauthenticated

Messages send through the Prelay netlayer can be viewed by the relay server,
modified and new messages inserted by the server or silently removed and thus
not delivered by the Prelay netlayer.

This means that you should only use the Prelay netlayers where you can place
high trust in the relaying server.

**** Lack of dialback within CapTP

When a node connects to another node in Goblins it provides the location where
it's accessable (i.e. the OCapN node address). One feature of OCapN is to ensure
only one connection exists to any given node, since the address provided by a
new incoming connection is never authenticated, this could lead to a denial of
service by a malicious actor claiming to be a node it is not.
** v0.11

*** Highlights

 - New time-traveling distributed debugger
 - Substantially improved documentation and docstrings
 - Several more actor-lib modules ported from Racket to Guile
 - Updates to OCapN code

*** New features and improvements
**** Time traveling distributed debugger

Goblins now includes a time traveling distributed debugger!  Yeah, you
heard that right!

The new Goblins debugger allows programmers to inspect and debug
distributed computations that happen across many vats (communicating
event loops.) The time travel feature allows for visiting past events
and inspecting program state at the time the event happened.

These tools are implemented as “meta-commands” for Guile's REPL,
complementing the tools that Guile already provides for sequential
debugging. In true Scheme fashion, this means that debugging happens
live, while the program is running, allowing programmers to fix bugs
and try again without having to stop and recompile/reboot their
program.

Blogpost, with examples:

  https://spritely.institute/news/introducing-a-distributed-debugger-for-goblins-with-time-travel.html

**** Documentation overhaul

Documentation has received a major overhaul in this release:

 - All key procedures now have docstrings, allowing for easier
   discovery of API usage during development
 - The manual's tutorial has been cleaned up for clarity and
   correctness
 - All core API procedures are now documented in the manual
 - All modules in the actor-lib are now documented in the manual
 - The manual now has an index, so finding procedures is much
   easier

**** More actor-lib libraries ported from Racket version of Goblins

 - =(goblins actor-lib pushdown)= is a simple pushdown automata
   implementation, useful for various kinds of state machines,
   especially in some games
 - =(goblins actor-lib opportunistic)= allows for proxying object
   behavior, opportunistically using =$= for near objects but using
   =<-= otherwise (known as select-swear in the Racket version)
 - =(goblins actor-lib simple-mint)= is a very simple example
   "bank" of sorts, ported from the example in
   [[http://erights.org/elib/capability/ode/index.html][An Ode to the Granovetter Diagram]]
 - =(goblins actor-lib let-on)= provides convenient code for promise
   resolution which strongly resembles =let= in Scheme, but with the
   behavior of =on= in Goblins

**** ,vats repl meta-command

It's now possible to list which vats are active via the =,vats=
REPL command.

**** OCapN / CapTP updates

Work has continued on Goblins' implementation of OCapN, the Object
Capability Network, the set of network abstractions which provide
the fluid experience of distributed networked programming.

Goblins' network architecture is still in active development; this
release breaks compatibility with the =0.10= release of Goblins.

***** Semantics closer to CapTP draft specification

Goblins' implementation of the OCapN version of CapTP (the Capability
Transfer Protocol) is aimed to be used as the starting point for
revisions for the OCapN group.  We have submitted a
[[https://github.com/ocapn/ocapn/pull/42][draft version of the protocol]] to the OCapN group.  Goblins 0.11 gets
us closer to the aims of that draft; we are hoping within the next few
releases to get full alignment and group consensus towards a unified
OCapN protocol, and the =0.11-goblins= release of OCapN and CapTP
shipped with Goblins gets us closer to that goal.

***** Add acyclic distributed GC support to CapTP

Goblins 0.11 now ships support for acyclic distributed garbage
collection, meaning that references to cooperative freeing of objects
shared over the network but no longer needed is possible

**** Facets use opportunistic synchronicity now

=(goblins actor-lib facet)= has been updated to use
=(goblins-actor-lib opportunistic)=.  In effect, this means that
the =#:sync?= keyword has been removed; facets now automatically
do the right thing.

*** Bug fixes

**** Backtraces which hung backtrace printing fixed

Previously certain backtraces would break and could hang Goblins.
This was due to a bug in Fibers in conjunction with Guile's backtrace
printer.  We have introduced a workaround which strips the frames
which would break Goblins.  The additional advantage of this fix is
that backtraces printed by Goblins are now significantly cleaner
with irrelevant information reduced.

**** Avoid explosion of threads on creation of new vats

Previously, new schedulers were accidentally made every time
new vats were made.  This would lead to an explosion of threads,
sometimes causing Guile to crash abruptly.  This bug is fixed.

**** Minor fixes

 - Bad reference to ^broken-ben in tutorial fixed
 - Nondeterministic test failures fixed
 - Sealers now print using correct port

*** Deprecations

 - =define-vat-run= is now deprecated, use =call-with-vat= or
   =with-vat= instead.

*** Known issues

 - Two machines simultaneously opening connections to each other on
   the OCapN network through Goblins fail to behave correctly.  This
   is known as the "crossed hellos" problem.  We are hoping to have
   this fixed in the next release.
 - We are still working towards full compatibility with the
   OCapN CapTP draft specification.

*** Thanks

David Thompson
Juliana Sims
Christine Lemmer-Webber
Jessica Tallon (Racket code ported to Goblins)
Vivianne Langdon
Geoffrey J. Teale

** v0.10

*** New features and improvements

**** OCapN support and interoperability with Racket Goblins

Guile Goblins has been updated to speak the OCapN protocol, finally
allowing Guile users to be able to easily make awesome
object-capability-secure peer-to-peer programs!  Furthermore, the
OCapN implementation is compatible with Racket Goblins, allowing both
flavors of Goblins to interopate with each other.

**** Vats are now record types supporting custom event loops

Vat objects used to be procedures that could be passed various
arguments to interact with the underlying fiber managing the vat.
Now, vat objects are a SRFI-9 record type and can be inspected using
procedures like =vat-name=.  =call-with-vat=/=with-vat= provide the
new interface for applying thunks within a vat.

Additionally, the default fibers vat implementation has been separated
from the core vat code, allowing custom vats to be implemented on top
of other asynchronous event loops.

See the manual for full details.

**** New ,enter-vat REPL meta-command

The new =,enter-vat= meta-command, which is automatically added to
REPLs when the =(goblins)= module is imported, allows evaluating code
within the context of a specific vat.  Errors generated within a vat
while evaluating an expression are propagated to the REPL for
debugging.

**** Expanded documentation

Much of the API surface has now been documented, including several
actor-lib modules and the OCapN API.  Additional examples and
instructions for setting up the Tor daemon have been added, as well.

**** More actor-lib libraries ported from Racket version of Goblins

New modules:

 - =(goblins actor-lib facets)= for attenuating capability methods
 - =(goblins actor-lib joiners)= for resolving one or multiple promises
   (=all-of= and =all-of*= anyway, =any-of= still needs to be ported)
 - =(goblins actor-lib pubsub)= for publish-subscribe
 - =(goblins actor-lib sealers)= for actor-based sealers/unsealers
 - =(goblins actor-lib ticker)= for a collection tool useful for game
   engines with many objects that need to be updated in a tick

*** Bug fixes

 - Message order across vats was previously incorrect after completion
   of a churn
 - =extend-methods= previously re-executed the extended code upon every
   invocation which was incorrect and error-prone
 - Various fixes to syrup library

*** Known issues

**** Fibers and backtrace printing

guile-fibers >= 1.1.0 has a bug related to exception handling that
causes backtrace printing to fail.  Goblins has worked around this by
omitting backtrace printing in certain situations.  See
https://github.com/wingo/fibers/issues/76 for more information.

** v0.6

This is the first release of Spritely Goblins on Guile.
There's not much to say in terms of "differences" from the previous
version of this package, since this is the first release.  Relative
to the [[https://docs.racket-lang.org/goblins/index.html][Racket version]], this library is at a fairly early stage.

That said, much of the functionality is here: actors/objects,
synchronous and transactional invocation via =$=, asynchronous
message passage via =<-=, promise pipelining, etc etc.

The main things that are missing are:

 - Easier ways to start up a vat for a more serious program (not just
   experimenting at the REPL)
 - A more fleshed out actor-lib
 - Most significantly, a working version of CapTP.

More soon.
