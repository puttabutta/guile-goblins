#+TITLE: Spritely Goblins: Guile edition

This is a port of [[https://gitlab.com/spritely/goblins][Spritely Goblins]] to [[https://www.gnu.org/software/guile/][Guile]].
Goblins is a distributed object programming environment.
It is both Spritely's core distributed networked technology, and
general enough to be used broadly.

Spritely, and Spritely Goblins, are developed under the stewardship
of the [[https://spritely.institute/][Spritely Institute]].  Support Spritely!

Goblins is a distributed programming system with local transactions
and following object capability security principles.  See [[https://spritely.institute/files/docs/guile-goblins/latest][the manual]]
for more details.

It's very early, so I'd caution against using it for anything
"production-facing" quite yet. :)

* Getting Goblins

** Using a package manager

Goblins is available in the following distributions:

*** GNU Guix

#+BEGIN_SRC sh
guix install guile-goblins
#+END_SRC

*** Homebrew (MacOS)

#+BEGIN_SRC sh
brew tap aconchillo/guile
brew install guile-goblins
#+END_SRC

** Building from a release tarball

If you're reading this file [[https://spritely.institute/goblins/][after downloading and extracting a release tarball]],
then these build instructions are for you!

Build requirements:

- [[https://www.gnu.org/software/guile/][GNU Guile]] >= 3.0
- [[https://github.com/wingo/fibers][guile-fibers]] >= 1.0.0
- [[https://gitlab.com/gnutls/guile][guile-gnutls]] >= 3.7.14

Additional runtime requirements:

- [[https://www.torproject.org/][Tor]] (any recent version) if you plan to use Onion netlayers

Goblins uses the GNU build system.  Once the required dependencies
have been installed, Goblins can be built by running the following
commands:

#+BEGIN_SRC sh
  ./configure GUILE=$(which guile)
  make
  sudo make install
#+END_SRC

Note that =./configure= uses a default prefix. You can check the load path with ~guile -c "(display %load-path)";~.
E.g. if this shows =(/usr/local/share/guile/3.0 /usr/local/share/guile/site/3.0 /usr/local/share/guile/site /usr/local/share/guile)=,
you can set the corrrect prefix with ~./configure GUILE=$(which guile) --prefix=/usr/local~.

* Contributing

** Checking out and building Goblins

By far the easiest way to hack on goblins is to develop using [[https://guix.gnu.org/][Guix]].

First check out Goblins:

#+BEGIN_SRC sh
  cd /my/projects/dir/
  git clone https://gitlab.com/spritely/guile-goblins.git
  cd guile-goblins
#+END_SRC

Now we can use =guix shell= to make a useful environment for us to
hack in, then run the autotools stuff.

#+BEGIN_SRC sh
  guix shell -m manifest.scm
  # In the guix environment, run:
  ./bootstrap.sh && ./configure && make check
#+END_SRC

If the tests pass, you're ready to go!  You can now hack this
project's files to your heart's content, whilst testing them from your
=guix environment=.

(You can also run =guix environment= with =--pure= if it accidentally
finds your system guile.)

** Setup for hacking on Goblins

*** Editor setup

You're going to want/need:

 - [[https://www.gnu.org/software/emacs/][GNU Emacs]]
 - [[https://www.nongnu.org/geiser/][Geiser]], for hacking on Guile

Then, at minimum, you'll want to put in your .emacs:

#+BEGIN_SRC emacs-lisp
(show-paren-mode 1)  ; Match parentheses
#+END_SRC

However you probably also want some combination of:

 - [[https://github.com/Fuco1/smartparens][smartparens]] (or [[https://www.emacswiki.org/emacs/ParEdit][paredit]], which is much more strict... use
   smartparens if uncertain)
 - [[https://github.com/Fanael/rainbow-delimiters][rainbow-delimeters]]
 - [[https://www.emacswiki.org/emacs/HighlightParentheses][highlight-parentheses]]

Now you'll have a nice hacking setup!  Be sure to read the Geiser
manual.

*** Running Guile with the path setup right

Simply run:

#+BEGIN_SRC sh
  ./live-guile.sh
#+END_SRC

Follow the instructions at the top, which should look something like:

#+BEGIN_SRC text
  ***********************************************
  ** Setting up Guile to listen on a socket... **
  **                                           **
  ** Connect in emacs via:                     **
  **   M-x geiser-connect-local <RET>          **
  **       guile <RET>                         **
  **       /tmp/guile-goblins.sock <RET>       **
  ***********************************************
#+END_SRC

Sounds like good advice!  Give that a try!


*** TEMPORARY: Live hacking "fix"

The traditional way to hack Guile is to use Geiser, both evaluating
from some =foo.scm= file/buffer with more "permanent" code via, for
example, the =C-x C-e= / =C-x C-b= / =C-x C-k= commands to evaluate an
expression, evaluate a whole buffer, or compile a buffer's content,
and switching back and forth between that and the (usually called
=*Geiser Guile REPL*=) experimental-hacking-and-debugging REPL.

This works with Goblins, but Geiser does some things to temporarily
redirect output/error ports when doing the file/buffer oriented
evaluation, and unfortunately this results in Goblins vats potentially
breaking due to spawning a separate thread which inherit these
temporarily redirected ports, which then are closed when the
evaluation succeeds.  We have a long-term plan to deal with this, but
in the meanwhile, whenever you start a Guile / Geiser hacking session,
first execute:

#+BEGIN_SRC scheme
  > (use-modules (goblins))
#+END_SRC

... from the =*Geiser Guile REPL*=.  Otherwise vats may mysteriously
"hang".  We know this is an annoyance!  It'll be fixed soon!

** Running test coverage

You should check any changes you make are tested. To help with this
there is a coverage tool you can run. You can run it by:

#+BEGIN_SRC sh
guile -L `pwd` --debug coverage.scm
#+END_SRC

NB: if you do not specify the --debug flag, no coverage data will be
generated.

This will generate a lcov.info file which you can then generate a
coverage report using the `genhtml` command from the lcov tools (you
may wish to look into the -o option as it generates lots of
files). The coverage currently includes certain guile modules which
you can ignore. E.g.

#+BEGIN_SRC sh
  mkdir /tmp/goblins-coverage
  genhtml -o /tmp/goblins-coverage/ lcov.info
  # There should now exist /tmp/goblins-coverage/index.html
#+END_SRC

** CI Image maintinance

The gitlab CI image is generated by guix based on the =ci-manifest.scm= file and
uploaded to gitlab. You can push a new image using the tool skopeo:

First, to get skopeo
#+BEGIN_SRC sh
guix shell skopeo
#+END_SRC

If this is your first time using the GitLab registry, you need to
login. This requires setting up a [[https://gitlab.com/-/profile/personal_access_tokens][GitLab personal access token]] with
read_api and write_registry permissions. Once you have a token, run:
#+BEGIN_SRC sh
skopeo login registry.gitlab.com
#+END_SRC:

Build and upload the image:
#+BEGIN_SRC sh
./upload-ci-image
#+END_SRC

** OCapN Test suite
If you're working on anything related to OCapN in Goblins, it's good to ensure
the test suite still passes. The test suite and how to run it is located [[https://github.com/ocapn/ocapn-test-suite][here]].

The test suite requires implementing certain objects and exporting
them at specific sturdyrefs. This has been done for you and you can
find those implementations within the
[[file:examples/ocapn-test-suite.scm][examples/ocapn-test-suite.scm]]. To run this you should run:

#+BEGIN_SRC sh
  $ ./pre-inst-env guile examples/ocapn-test-suite.scm 
Connect test suite to: <sturdyref>
#+END_SRC

Copy the sturdyref it gives you and then in the OCapN test suite you
can run it with this command:
#+BEGIN_SRC sh
  $ python3 test_runner.py --captp-version=goblins-0.12 <sturdyref>
#+END_SRC

Since this is using the tor onion netlayers, it is very slow, and
typically takes many minutes to run all the tests.

/Note: the =--captp-version=goblins-0.12= is important as goblins
lists this as the CapTP version it uses. Since OCapN is currently in
development, this allows goblins to differentiate between different
goblins versions as the CapTP specification evolves./
* A note on licensing

Spritely Goblins is released under [[file:./LICENSE.txt][Apache v2]].

Note that this is not anti-copyleft positioning; copyleft is a viable
strategy for preserving user freedom.  In the case of Spritely, we are
aiming for a wide adoption of ideas and protocols, so it's a /different/
strategic choice.
