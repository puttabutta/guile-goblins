@uref{https://github.com/ocapn/ocapn, OCapN} stands for ``Object
Capability Network'' and is a suite of protocols for communication
between distributed, networked objects.  In other words, it's how
Goblins actors can talk to actors running on another computer.

OCapN uses the @uref{http://erights.org/elib/distrib/captp/index.html,
Capability Transport Protocol (CapTP)} as the core protocol for
enabling distributed programming in a mutually suspicious network.  It
originated in the @uref{http://www.erights.org/, E programming
language} but has since appeared in @uref{https://capnproto.org/,
Cap'N Proto}, @uref{http://waterken.sourceforge.net/, Waterken},
@uref{https://agoric.com/, Agoric}'s
@uref{https://github.com/Agoric/agoric-sdk/tree/master/packages/SwingSet,
SwingSet}, and now Goblins.

@strong{CAUTION}: CapTP support is still in its early days.  Please do
not rely on the protocol being stable at this time.

@menu
* CapTP The Capability Transport Protocol::
* Launching a Tor daemon for Goblins::
* Example Two Goblins programs chatting over CapTP via Tor::
* Using the CapTP API::
* Netlayers::
@end menu

@node CapTP The Capability Transport Protocol
@section CapTP: The Capability Transport Protocol

OCapN's CapTP has an abstract ``netlayer'' interface, allowing
communication over many different protocols ranging from Tor Onion
Services to IBC to I2P to libp2p to perhaps carrier pigeons with
backpacks full of encrypted MicroSD cards.  Currently, Goblins only
supports the Tor Onion Services netlayer.

Benefits of CapTP:

@itemize
@item
Invocation of remote objects resembles the same as with local objects.
You can use @code{<-} and @code{on} which means that most programs
that were originally designed for local-only computing
naturally scale out to a networked environment.
@item
All this is done while mostly hiding the abstraction of the networked
protocol from the user.
@item
Object capability security is upheld.  A remote node cannot make
use of any capability that has not been handed to it.
@item
Live object references between CapTP endpoints are incredibly cheap,
merely represented as integers on each side.  This keeps message sizes
small and efficient.
@item
CapTP provides (acyclic) distributed garbage collection.  Remote
nodes can indicate when they no longer need an object and the
node locally containing that reference can reclaim it if
appropriate.
@item
Promise pipelining means that messages can be sent to the resolution
of a promise before that resolution actually occurs.  Over the network
this is represented as a pipeline of messages.  This can reduce round
trips significantly, which is a big win.

To re-quote Mark S@. Miller:

@quotation
Machines grow faster and memories grow larger.  But the speed of light
is constant and New York is not getting any closer to Tokyo.
@end quotation
@item
CapTP is written independently of network transport abstractions.  It
can be run over local Unix domain sockets, an OpenSSL connection, Tor
Onion Services, or something custom.  CapTP operates under the
assumption of secure pairwise channels; the layer which provides the
pairwise channels is called ``MachineTP'' and can be written in a
variety of ways.  A simple MachineTP abstraction is provided, but
presently still requires some manual wiring.  In the future, even
simpler abstractions will be provided which cover most user needs.
@end itemize

All of these combine into an efficient and powerful protocol that,
most powerfully of all, the author of Goblins programs need not
think too much about.  This circumvents the usual years and years of
careful message structure coordination and standardization for many
kinds of distributed programs, which can simply be written as normal
Goblins programs rather than bespoke protocols.

Limitations of CapTP:

@itemize
@item
Goblins' CapTP provides @emph{acyclic} distributed garbage collection.
Cycles between servers are not automatically recognized.  Full
@uref{http://erights.org/history/original-e/dgc/, cycle-collecting
distributed garbage collection} has been written, but requires special
cooperation from the garbage collector that is not available in
Guile (or in most languages).
@item
Goblins' CapTP does not do anything about memory usage or resource
management on its own.  (Features for this will come as Spritely
sub-projects in the future.)
@item
While CapTP in theory routes capabilities to specific remote objects,
since the network is mutually suspicious one can't assume that the
remote end isn't conspiring some way to hand those capabilities to
other unexpected objects.  The right way to think about this
from an object capability perspective is that a remote misbehaving
node is equivalent to a misbehaving object with the surface area of
the entire node.
@item
@strong{TEMPORARY}: The API is heavily in flux.
@end itemize

@node Launching a Tor daemon for Goblins
@section Launching a Tor daemon for Goblins

In order for Goblins' Onion Service netlayer to work, it needs to talk
to a Tor daemon.  Goblins can set up and manage the Onion Services
itself once it connects to the Tor daemon.  So far it seems like
running a single Goblins Tor process ``as your user'' and letting
various Goblins processes connect to it is the easiest option.

Here's an example configuration file template (replace @code{<user>}
with your actual username):

@example
DataDirectory /home/<user>/.cache/goblins/tor/data/
SocksPort unix:/home/<user>/.cache/goblins/tor/tor-socks-sock RelaxDirModeCheck
ControlSocket unix:/home/<user>/.cache/goblins/tor/tor-control-sock RelaxDirModeCheck
Log notice file /home/<user>/.cache/goblins/tor/tor-log.txt
@end example

Write this configuration to @file{~/.config/goblins/tor-config.txt}.

Create the necessary cache directory:

@example
mkdir -p ~/.cache/goblins/tor/data
@end example

Now launch the Tor daemon:

@example
tor -f ~/.config/goblins/tor-config.txt
@end example

@node Example Two Goblins programs chatting over CapTP via Tor
@section Example: Two Goblins programs chatting over CapTP via Tor

Before diving into API details, let's take a look at a couple of small
example programs to demonstrate that this stuff works.

Here's our client script named @file{captp-alice.scm}:

@lisp
(use-modules (fibers conditions)
             (fibers operations)
             (goblins)
             (goblins ocapn ids)
             (goblins ocapn captp)
             (goblins ocapn netlayer onion)
             (ice-9 match))

;; Create a CapTP router using Tor Onion Services.
(define node-vat (spawn-vat))
(define onion-netlayer
  (with-vat node-vat
    (spawn ^onion-netlayer)))
(define mycapn
  (with-vat node-vat
    (spawn-mycapn onion-netlayer)))

;; Convert the command-line argument into a sturdyref for Bob.
(define bob-uri
  (match (command-line)
    ((_ uri) uri)))
(define bob-sref (string->ocapn-id bob-uri))

;; Create a vat for Alice and enliven Bob.
(define a-vat (spawn-vat))
(define bob-vow
  (with-vat a-vat
    (<- mycapn 'enliven bob-sref)))

;; Send a message to Bob and print the response.
(define done? (make-condition))
(with-vat a-vat
  (on (<- bob-vow "Alice")
      (lambda (response)
        (format #t "Alice heard back: ~a" response)
        (signal-condition! done?))))

;; Wait until Alice has received a response before exiting.
(perform-operation (wait-operation done?))
@end lisp

Traditionally, Alice talks to Bob.  So, now we need Bob.  Save the
following server script as @file{captp-bob.scm}:

@lisp
(use-modules (fibers conditions)
             (fibers operations)
             (goblins)
             (goblins ocapn ids)
             (goblins ocapn captp)
             (goblins ocapn netlayer onion))

;; Bob is going to be a greeter that says hello to other people.
(define (^greeter _bcom our-name)
  (lambda (your-name)
    (format #f "Hello ~a, my name is ~a!" your-name our-name)))

;; Create Bob in their own vat.
(define b-vat (spawn-vat))
(define bob (with-vat b-vat (spawn ^greeter "Bob")))

;; Create a CapTP router using Tor Onion Services.
(define node-vat (spawn-vat))
(define-values (onion-netlayer private-key service-id)
  (with-vat node-vat
    (spawn ^onion-netlayer)))
(define mycapn
  (with-vat node-vat
    (spawn-mycapn onion-netlayer)))

;; Create a sturdyref for Bob and print it.
(define bob-sref
  (with-vat node-vat
    ($ mycapn 'register bob 'onion)))
(format #t "Bob's sturdyref: ~a"
        (ocapn-id->string bob-sref))

;; Hold the process open indefinitely.
(perform-operation (wait-operation (make-condition)))
@end lisp

Okay, time to try it out!  Open up two terminals, and in the first one
run:

@example
guile captp-bob.scm
@end example

This will print out a captp ``sturdyref'' URI to the terminal.  Copy
it and paste it in the second terminal as the argument to @code{guile
captp-alice.scm}:

@example
guile captp-alice.scm <STURDYREF-GOES-HERE>
@end example

Tor Onion Services can take a while to establish, but eventually you
should see:

@example
Alice heard back: Hello Alice, my name is Bob!
@end example

If you see the same output then your connection succeeded!  Yay! The
next sections will get into the details of how to use the CapTP API@.

@node Using the CapTP API
@section Using the CapTP API

Goblins uses an actor to communicate over CapTP@.  This actor can be
spawned using the @code{spawn-mycapn} procedure from the
@code{(goblins ocapn captp)} module:

@lisp
> (define mycapn (spawn-mycapn netlayer))
@end lisp

Where @code{netlayer} refers to a CapTP netlayer object.  See the next
section for details on available netlayers.

Local actors need to be registered so that they can receive messages
over CapTP@.  Registering an actor generates a CapTP identifier for
that actor:

@lisp
> (define alice (spawn ^greeter "Alice"))
> (define alice-captp-id ($ mycapn 'register alice 'onion))
@end lisp

To use this actor on another node, that node needs a
``sturdyref'' to the actor.  A sturdyref is a special URI that
uniquely identifies the actor on the network.  The
@code{ocapn-id->string} procedure from the @code{(goblins ocapn ids)}
module can generate such a URI:

@lisp
> (ocapn-id->string community-sref)
=> "ocapn://jv4arqqwntmxs3rljkx3ao5bucci37rxxb575vypovntyve76mmyf4ad.onion/s/nGxw4_p6s91IF-R5r6MMYsZzoByODX46MV7AWTApX5s"
@end lisp

To generate a Guile URI object instead of a string, use
@code{ocapn-id->uri} instead.

On the other node, the sturdyref can be ``enlivened'' to create a
promise that, when resolved, yields a remote object:

@lisp
> (define vow ($ mycapn 'enliven "ocapn://..."))
> vow
=> #<local-promise>
> (on vow
      (lambda (obj)
        (display obj)
        (newline)))
; #<remote-object>
@end lisp

Sending messages to remote objects using @code{<-} looks the same as
sending messages to local objects.  The message will be transparently
sent over CapTP to the remote object!

@enumerate
@item
@anchor{Keeping a persistent sturdyref for an object}Keeping a persistent sturdyref for an object


When an object is registered, it is given a ``swiss num'' (object
specific part of the sturdyref). This can be extracted from the ocapn
id:

@lisp
> (define swiss-num (ocapn-sturdyref-swiss-num alice-captp-id))
> swiss-num
=> #vu8(156 108 112 227 250 122 179 221 72 23 228 121 175 163 12 98 198 115 160 28 142 13 126 58 49 94 192 89 48 41 95 155)
@end lisp

Often it is desirable to have an object have a persistent sturdyref
between sessions; to do this, you need to do two things:

@itemize
@item
Register the object at a specific swiss num
@item
Restore the netlayer (see the netlayer section)
@end itemize

To install the object at a given swiss num, get the nonce
registry. This is an object which holds all of the swiss nums to each
registered object in mycapn. The following is how this can be done:

@lisp
> (define registry ($ mycapn 'get-registry)) ;; nonce registry
> (define alice-captp-id ($ registry 'register alice swiss-num))
@end lisp
@end enumerate

@node Netlayers
@section Netlayers

While OCapN's CapTP provides the glue of object-to-object remote
invocation machinery, OCapN's netlayers are an abstraction over message
transmission protocols which give OCapN the flexibility to use a
number of underlying networking substrates.

@node Tor Onion Services
@subsection Tor Onion Services

Assuming a Tor daemon is running, a Goblins Onion netlayer can be
created by spawning the @code{^onion-netlayer} object from the
@code{(goblins ocapn netlayer onion)} module:

@lisp
> (define onion-netlayer
    (spawn ^onion-netlayer))
@end lisp

That netlayer can then be used to spawn an OCapN router actor:

@lisp
> (define mycapn (spawn-mycapn onion-netlayer))
@end lisp

Each the @code{^onion-netlayer} is spawned it generates a new
@code{private-key} and @code{service-id}.  If you wish for these to
persist, they need to be stored (securely, as anyone with them has
the capability to host at them). This can be done by using Goblins'
persistence system, an example of this is:

@lisp
> (define-values (ocapn-vat onion-netlayer onion-mycapn)
    (spawn-persistent-vat
     (make-persistence-env #:extends (list onion-netlayer-env captp-env))
     (lambda ()
       (define netlyaer
         (spawn ^onion-netlayer))
       (values netlayer
               (spawn-mycapn netlayer)))
     (make-syrup-store "ocapn.syrup")))
@end lisp

@node TCP + TLS
@subsection TCP + TLS

The TCP + TLS netlayer is available as a simpler option than Tor or
other peer-to-peer networks.  Direct connections are made over
standard TCP sockets and all traffic is encrypted with self-signed TLS
keys.  A new netlayer instance can be spawned with the
@code{^tcp-tls-netlayer} object from the @code{(goblins ocapn
netlayer tcp-tls)} module.  The TCP + TLS netlayer is often used in
conjunction with, and as a foundation for, the @ref{Prelay} netlayer.

@lisp
> (define netlayer (spawn ^tcp-tls-netlayer "example.com"))
> (define mycapn (spawn-mycapn netlayer))
@end lisp

Users of this netlayer can either bring their own X.509 key and
certificate by importing them from the file system, or let Goblins
automatically generate them.  The @code{^tcp-tls-netlayer} is
persistence aware meaning that it can be persisted and restored using
Goblins' persistence system. Here's an example of this:


@lisp
(define-values (ocapn-vat tcp-tls-netlayer tcp-tls-mycapn)
  (spawn-persistent-vat
   (make-persistence-env #:extends (list tcp-tls-netlayer-env))
   (lambda ()
     (define netlayer
       (spawn ^tcp-tls-netlayer "example.com"))
     (values netlayer
             (spawn-mycapn netlayer)))
   (make-syrup-store "ocapn.syrup")))
@end lisp

The @emph{current major limitation} of this netlayer is the lack of
both ``store and forward'' and NAT hole punching support.  Most
machines that are not dedicated servers are not accessible from the
public Internet due to the presence of a NAT gateway between the local
network and the Internet.  For this reason the most common way to use
the TCP + TLS netlayer is as an underlying foundation for the
@ref{Prelay} netlayer.

Without using the @ref{Prelay} netlayer, the only way to reach peers
behind NAT gateways is if those peers have setup the necessary port
forwarding configuration on their local network's firewall and have
configured the netlayer to use their public IP as the host name.  If
your application would benefit from a more peer-to-peer netlayer,
consider using the @ref{Tor Onion Services} netlayer instead, which
is more peer-to-peer at the cost of high latency.

Sturdyrefs for this netlayer use a SHA-256D hash of the TLS
certificate as the node identifier.  The address and port where the
node can be found on the network are encoded as hints.

Also, note that the current version of this netlayer is robust against
man-in-the-middle attacks in the sense that an attacker cannot
manipulate or decode communication occurring over the TLS encrypted
stream.  However, no attempt has been made to prevent network
monitoring attacks where an attacker provides a sturdyref with hints
pointing at an intermediary server where the attacker can observe
@emph{when} and @emph{how much} traffic is occurring, even though they
cannot interpret the contents.  In addition, the attacker could choose
to sever the connection when it is convenient for them.  This is
generally the case already on nation-state level attacks against TLS,
but is more readily available as a possible avenue of attack here.  In
the future, hints might be signed in order to prevent this.

@deffn {Constructor} ^tcp-tls-netlayer host [#:port] [#:max-connections 32] @
                                       [#:key] [#:cert]

Construct an TCP-TLS netlayer actor.  @var{host} specifies the
hostname that appears in the OCapN sturdyrefs that use this netlayer.

If @var{port} is specified, the netlayer will listen for incoming
connections on that port or throw an error if the port is already in
use.  If @var{port} is not specified, an open port will be chosen
automatically.

@var{max-connections} specifies the number of peers that may be
connected to the netlayer at any given time.

@var{key} and @var{cert} specify the X.509 private key and certificate
to use for encrypting connections.  If one or both are unspecified,
they will be automatically generated provided that the version of
Guile-GnuTLS is new enough to do so.
@end deffn

@deffn {Procedure} generate-tls-private-key

Return a freshly generated X.509 private key.

@end deffn

@deffn {Procedure} generate-tls-certificate key

Return a freshly generated X.509 certificate for @var{key}.

@end deffn

@defvr {Persistence Environment} tcp-tls-netlayer-env
@end defvr
                                                
@node libp2p
@subsection libp2p

The libp2p netlayer communicates over the @url{https://libp2p.io,libp2p}
networking stack, which is designed to provide secure peer-to-peer
connections over a variety of networking protocols.
Goblins relies upon an external server, the @url{https:/
/gitlab.com/spritely/go-libp2p-daemon,Go libp2p daemon}, to access the
libp2p network.  Refer to the
@url{https://gitlab.com/spritely/go-libp2p-daemon,daemon's documentation}
for setup instructions.

@deffn {Constructor} ^libp2p-netlayer [private-key] [our-location]
                                      [#:control-path] [#:path]

Spawning this constructor will produce a new instance of the libp2p
netlayer.

The @var{private-key} of type string and @var{our-location} of type
OCapN node can be used to specify an existing private key and location
to be used by the netlayer. If these are not specified, they are
generated by the netlayer. The netlayer is persistence aware so these
do not normally need to be specified manually.

@var{control-path} when specified the path to the unix socket where
the libp2p daemon is running. If not specified it will default to
@file{/tmp/libp2p-control.sock}

@var{path} is the path to a directory where Goblins can create new
sockets. These sockets are used for new connections for Goblins and the
libp2p daemon to communicate over. It defaults to
@file{/tmp/goblins-libp2p}

@end deffn

@deffn {Procedure} ocapn-node->libp2p-multiaddr node

When provided an OCapN node, it will convert this to a multiaddr string
used by libp2p.

@end deffn

@deffn {Procedure} libp2p-multiaddress->ocapn-node multiaddrs

When given a string of multiaddrs, it will convert it into an OCapN node.

@end deffn

@defvr {Persistence Environment} libp2p-netlayer-env
@end defvr

@node Prelay
@subsection Prelay (Unencrypted, v0)

The purpose of the prelay netlayer is to permit connections between two
parties which cannot feasibly directly connect.  For instance, some
users may be able to feasibly connect unidirectionally via the
@ref{TCP + TLS} netlayer to a ``public'' node, but may not be
themselves reachable over a consistent IP address.  For this reason,
the Prelay netlayer exists.

The surprising thing about the Prelay netlayer is that it is a netlayer
which runs *over another netlayer* by passing messages through an
intermediate object, which itself speaks to other intermediate objects
routing to other likely isolated users, bridging those users with the
illusion of being direct peers.  (The users need not use relays on the
same node; ``federation'' is achieved automatically through the
underlying OCapN netlayer's connectivity properties.)

The primary limitation of the prelay netlayer at present is that
messages are delivered in cleartext through the chosen relay,
unauthenticated, which means the relay has the power to both snoop on
and modify messages.  A fully end-to-end-encrypted Relay netlayer is on
schedule to replace the present unencrypted, unauthenticated design.
At present the prelay netlayer has characteristics similar to IRC,
vanilla ActivityPub, and many multiplayer game systems which also tend
to rely on trusting a provider, but we would like to (and will!) do
better in the future.

Some simpler APIs for the prelay netlayer are coming, particularly a
convenient daemon for managing prelay nodes for users.

In the meanwhile, there are two essential APIs.  The first is run on
relay nodes, which act as the routers:

@deffn {Procedure} spawn-prelay-pair enliven

Spawn a pair of prelay objects: the @var{endpoint} (public) and
@var{controller} (private).  The endpoint permits other nodes to send
messages, and the controller permits sending messages to other nodes.

@var{enliven} is a capability to enliven a sturdyref, and returns a
promise.  Probably a facet of the MyCapN object.

Returns two values to its continuation, the @var{endpoint} and
@var{controller} respectively.

@end deffn

In order for the @var{endpoint} to be useful as a public identity, a
sturdyref must be made for it and then converted with the following
procedure:

@deffn {Procedure} prelay-sturdyref->prelay-node prelay-endpoint-sref

This transforms @var{prelay-endpoint-sref} into an OCapN prelay node
identifier and returns that object and identifies this node on the
network.  (This is done by transforming and serializing relevant
information about the sturdyref into a data structure that is then
packed into the @code{ocapn-node-designator} object, but this detail
is not particularly important for most users.)

@end deffn

This transformation can be reversed with the following procedure:

@deffn {Procedure} prelay-node->prelay-sturdyref prelay-node

Do the reverse of @var{prelay-sturdyref->prelay-node}, which is to say,
transform the node back into a sturdyref which represents the endpoint
which handles routing messages for this node.

Whew, that's a lot!  The above procedures are essential for setting up
prelay-as-relay-host infrastructure, but setting up a prelay-using
netlayer is much simpler:

@end deffn

@deffn {Constructor} ^prelay-netlayer bcom prelay-endpoint-sref prelay-controller

Constructs the prelay netlayer which lives on the client.

Takes two arguments at spawn time.  @var{prelay-endpoint-sref} is a
sturdyref of the endpoint we will use to communicate with.
@var{prelay-controller} is a live, probably remote, reference which we
use to pilot the relay we communicate through.

@end deffn

@defvr {Persistence Environment} prelay-env
@end defvr

@node Prelay Utils
@subsection Prelay Utils

The @ref{Prelay} netlayer is built around there being a Prelay server which
accepts connections, allowing the controlling user to open new connections
and facilitates passing messages back and forth for established
connections. This Prelay server must be setup somewhere accessible on
the network it operates.

Goblins comes with a set of Prelay utils which are designed to offer one
way to setup the server with multiple accounts. There are many ways this
could be done, however other Prelay server designs are left to the
reader to build. The Prelay utils are available in the
@code{(goblins ocapn netlayer prelay-utils)} module, and an example of how
to use them to run a daemon can be found in @code{examples/prelay-server.scm},
as shipped with Spritely Goblins.

@code{(goblins ocapn netlayer prelay-utils)} provides the following functions:

@deffn {Constructor} ^prelay-admin bcom enliven register

This constructs an admin actor to manage prelay accounts. This is to be
used on the ``server'' side.

It takes two arguments: @var{enliven} which is a facet object that when
provided the symbol @code{'enliven} followed by a OCapN sturdyref
returns a vow of the enlivened sturdyref. It also takes @var{register}
which when provided an object will make that object available at a given
sturdyref and return that to the caller. Both of these are likely facets
of the Mycapn object.

The admin object itself has two methods, those are:

@itemize
@item
@code{add-account} which takes a name of an account and returns a
sturdyref. This sturdyref is designed to be given to the user who is
controlling the relay account. The user can then enliven said
sturdyref and send a message to the enlivened object to get the prelay
endpoint and prelay controller objects.

@item
@code{get-accounts} which provides a list of all account names
registered with this prelay admin.
@end itemize

@end deffn

@deffn {Procedure} fetch-and-spawn-prelay-netlayer account-setup-sref [#:netlayer] [#:mycapn]

This takes a account setup sturdyref retrieved from the
@code{add-account} method on the Prelay admin and returns a
promise resolving to a freshly spawned Prelay netlayer.
This is to be used on the ``client'' side.

By default when using the relay over @ref{Tor Onion Services} netlayer
or @ref{TCP + TLS} netlayer it will setup a new netlayer to use to speak
with the relay over. If a different netlayer is to be used, or any of
the default options need changing, the netlayer should be setup beforehand
and passed in through the @var{#:netlayer} argument.

By default it will create a new mycapn, however if one already exists
for the netlayer specified in @var{#:netlayer}, then you can pass that
in with the @var{#:mycapn} argument.

@end deffn

@defvr {Persistence Environment} prelay-utils-env
@end defvr
